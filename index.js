'use strict'

var forever = require('forever-monitor');
var Primus = require('primus');
var child_process = require('child_process');

var App = require('./app.js');

/**/

var app = new App({
	port: 9000
});

var foundVLC = false;

child_process.exec('which vlc', function(err, stdout, stderr) {
    if ( err || stderr.indexOf('not found') >= 0  || stdout.indexOf('not found') >= 0 ) {
        foundVLC = false;
    } else {
        foundVLC = true;
    }

    if (foundVLC) {
        runVLCServers();
    } else {
        handleWarning();
    }
});

function handleWarning() {
    console.warn("It looks like you don't have VLC installed or VLC is not working. You won't be able to get video streaming.");
}

function runVLCServers() {
    /* Run VLC servers */
    var vstream1 = forever.start(['vlc', '-I', 'dummy', '-v', '--live-caching', '0', 'udp://@:9001', ':sout=#transcode{vcodec=MJPG,vb=1000}:standard{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9002}}'], {
        max : 20,
        silent : false
    }).on('error', handleWarning);

    var vstream2 = forever.start(['vlc', '-I', 'dummy', '-v', '--live-caching', '0', 'udp://@:9003', ':sout=#transcode{vcodec=MJPG,vb=1000}:standard{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9004}}'], {
        max : 20,
        silent : false
    }).on('error', handleWarning);

    var vstream_tracker = forever.start(['vlc', '-I', 'dummy', '-v', '--live-caching', '0', 'udp://@:9005', ':sout=#standard{access=http{mime=multipart/x-mixed-replace;boundary=--7b3cc56e5f51db803f790dad720ed50a},mux=mpjpeg,dst=:9006}}'], {
        max : 20,
        silent : false
    }).on('error', handleWarning);

    var astream1 = forever.start([ 'cvlc', 'rtp://:9007', ':sout=#standard{access=http,mux=mp3,dst=:9008}', '-I', 'dummy' ], {
        max : 20,
        silent : false
    });

}