
# Getting Started

1. Run `npm install .`
2. Run `node index.js`

Middleman should be running on port 9000. You can edit this in `index.js`

# Packets

## Rover => Middleman (Initial Connection)
```
{
	intent: 'REGISTER',
	info: {
		entity: 'cortex',
		password: 'destroyeveryone'
	}
}
```

## Browser => Rover
```
{
	target: <string>, // arm, navi, tracker, etc
	command: <string>, // command name
	data: <object> // parameters / data
}
```

## Rover => Browser
```
{
	target: <string>, // arm, navi, tracker, etc
	data: <object>, // map of variables and their values
}
```

## On Rover Connection / Disconnection
```
{
	target: <string>, // arm, navi, tracker,
	connection: "connected" / "disconnected"
}
```