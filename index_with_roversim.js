// THIS IS FOR TESTING NOT FOR PRODUCTION!

'use strict'

var Primus = require('primus');
var App = require('./app.js');

/* Fake Rover */

var Socket = new Primus.createSocket()
var roverSocket = new Socket("http://localhost:9000");
roverSocket.on('data', function(data) {
	console.log("Received on Fake Rover!");
	console.log(data);
});

// setInterval(function() {
// 	roverSocket.write({
// 		target: "NAVI", 
// 		longitude: 10
// 	});
// }, 500);

roverSocket.write({
	intent: 'REGISTER',
	info: {
		entity: 'cortex',
		password: 'destroyeveryone'
	}
});

/**/

var app = new App({
	port: 9000
});
