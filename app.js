'use strict'

var Primus = require('primus');
var fs = require('fs');

var currentDate = new Date();
var dateString = currentDate.toDateString().replace(/[^A-Za-z0-9]/g, "_");

module.exports = class App {
	constructor(options) {

		var options = options || {};
		var primusConfig = {port: options.port, transformer: 'websockets'};

		this.model = {};
		this.pings = {};

		this.sparkMap = {};
		this.primus = Primus.createServer((spark) => {
			console.log("Connection...");

			spark.on('data', (data) => {
				console.log ("[" + (new Date()).toString() + "] Received from " + (data.target || "N/A"));
				console.log (data);
				
				if (data.target == "model") {
					fs.appendFileSync('log-' + dateString + '.txt', data.message, 'utf8');
					// console.log(data);

					try {
						var model = JSON.parse(data.message);
						for (var key in model) {
							this.model[key] = model[key];
						}
					} catch(e) { console.log("Malformed JSON object"); }

					this.sendToBrowsers({model: data.message});
				}

				// A browser page has connected.
				if (data.connection && data.connection == "connected" && data.target) {
					this.linkSpark(spark, data.target);

					// Tell rovercore.
					this.sendToRover({
						target: spark.target,
						connection: "connected"
					});

					// Send the current model.

					spark.write({
						model: JSON.stringify(this.model)
					});

					for (var id in this.pings) {
						var ping = this.pings[id];

						spark.write({model: JSON.stringify({
							executeCommand: {
								name: "addPing",
								id: ping.id,
								long: ping.long,
								lat: ping.lat,
								text: ping.text
							}
						})});
					}

					return;
				}

				// RoverCore is in the house!
				if (data.intent && data.intent == "REGISTER" && data.info.entity == "cortex" && data.info.password == "destroyeveryone") {
					// Store RoverCore's spark for later reference.
					this.rover = spark;
					this.model.roverOnline = {value: true};
					this.sendToBrowsers({model: JSON.stringify({roverOnline: {value: true}})});

					console.log("Rover has connected.");
				} else {

					// We received data from the rover?
					if (this.rover == spark) {
						// Send to all browser pages
						this.sendToTargets(data.target, data);

					// We received data from a browser page?
					} else {
						// Send to rover
						if (this.intersectCommands(data)) return;

						this.sendToRover(data);
					}

				}
			});

			spark.on('end', () => {
				// A brwoser page has disconnected.
				if (spark.target && spark != this.rover) {
					// Tell rovercore.
					this.sendToRover({
						target: spark.target,
						connection: "disconnected"
					});

					this.unlinkSpark(spark);
				}

				if (this.rover == spark) {
					this.model.roverOnline = {value: false};
					this.sendToBrowsers({model: JSON.stringify({roverOnline: {value: false}})});

					console.log("Rover has disconnected.");
				}
			})

		}, primusConfig);

	}

	linkSpark(spark, target) {
		spark.target = target
		this.sparkMap[target] = this.sparkMap[target] || [];
		if (this.sparkMap[target].indexOf(spark) == -1) {
			this.sparkMap[target].push(spark);
		}
	}

	unlinkSpark(spark) {
		this.sparkMap[spark.target].splice(this.sparkMap[spark.target].indexOf(spark), 1);
	}

	sendToBrowsers(data) {
		for (var target in this.sparkMap) {
			for (var i in this.sparkMap[target]) {
				// console.log(this.sparkMap[target]);
				this.sparkMap[target][i].write(data);
			}
		}
	}

	sendToTargets(target, data) {
		this.sparkMap[data.target] = this.sparkMap[data.target] || [];
		for (var i in this.sparkMap[data.target]) {
			this.sparkMap[data.target][i].write(data);
		}
	}

	sendToRover(data) {
		if (this.rover) {
			this.rover.write(data);
		}
	}

	intersectCommands(data) {
		if (data.target == "Pinger") {
			if (data.command.command == "add") {
				this.sendToBrowsers({model: JSON.stringify({
					executeCommand: {
						name: "addPing",
						id: data.command.id,
						long: data.command.long,
						lat: data.command.lat,
						text: data.command.text
					}
				})});

				this.pings[data.command.id] = {
					id: data.command.id,
					long: data.command.long,
					lat: data.command.lat,
					text: data.command.text
				}
			}

			if (data.command.command == "remove") {
				this.sendToBrowsers({model: JSON.stringify({
					executeCommand: {
						name: "removePing",
						id: data.command.id,
					}
				})});

				if (this.pings[data.command.id]) {
					delete this.pings[data.command.id];
				}
			}

			return true;
		}

		return false;
	}
}
